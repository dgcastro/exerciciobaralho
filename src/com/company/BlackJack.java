package com.company;

import java.util.Scanner;

public class BlackJack {
    private String Jogador;
    private Rodada melhorRodada;

    public BlackJack(String jogador) {
        Jogador = jogador;
        melhorRodada = new Rodada(0);
    }

    public Rodada getMelhorRodada() {
        return melhorRodada;
    }

    public void novaRodada(){
        Rodada novaRodada = new Rodada();
        int decisao;
        System.out.println("Deseja comprar uma nova carta?\nPressione 1 para comprar ou 0 para manter a mão");
        Scanner scanner = new Scanner(System.in);
        decisao = scanner.nextInt();
        while(decisao  != 0 && novaRodada.getValor()<=21) {
            if (novaRodada.hit()) {
                System.out.println("Você tem:"+novaRodada.getValor()+" pontos. Deseja Continuar?");
                System.out.println("Deseja comprar uma nova carta?\nPressione 1 para comprar ou 0 para manter a mão");
                decisao = scanner.nextInt();

            }else{
                System.out.println("Sua mão 'estorou' com: "+novaRodada.getValor()+" pontos.");
            }
        }
        if(novaRodada.getValor()<=21)
            melhorRodada = novaRodada;
    }
}
