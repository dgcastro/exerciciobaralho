package com.company;

import java.util.Collections;
import java.util.LinkedList;

public class Baralho {
    private LinkedList<Carta> cartas;

    public Baralho() {

        cartas = new LinkedList<Carta>();
        /*
            A seguir estão as cartas pertencentes a este baralho.
        */
        cartas.add(new Carta("A", "Ouros", 1));
        cartas.add(new Carta("A", "Espadas", 1));
        cartas.add(new Carta("A", "Copas", 1));
        cartas.add(new Carta("A", "Paus", 1));

        cartas.add(new Carta("2", "Ouros", 2));
        cartas.add(new Carta("2", "Espadas", 2));
        cartas.add(new Carta("2", "Copas", 2));
        cartas.add(new Carta("2", "Paus", 2));

        cartas.add(new Carta("3", "Ouros", 3));
        cartas.add(new Carta("3", "Espadas", 3));
        cartas.add(new Carta("3", "Copas", 3));
        cartas.add(new Carta("3", "Paus", 3));

        cartas.add(new Carta("4", "Ouros", 4));
        cartas.add(new Carta("4", "Espadas", 4));
        cartas.add(new Carta("4", "Copas", 4));
        cartas.add(new Carta("4", "Paus", 4));

        cartas.add(new Carta("5", "Ouros", 5));
        cartas.add(new Carta("5", "Espadas", 5));
        cartas.add(new Carta("5", "Copas", 5));
        cartas.add(new Carta("5", "Paus", 5));

        cartas.add(new Carta("6", "Ouros", 6));
        cartas.add(new Carta("6", "Espadas", 6));
        cartas.add(new Carta("6", "Copas", 6));
        cartas.add(new Carta("6", "Paus", 6));

        cartas.add(new Carta("7", "Ouros", 7));
        cartas.add(new Carta("7", "Espadas", 7));
        cartas.add(new Carta("7", "Copas", 7));
        cartas.add(new Carta("7", "Paus", 7));

        cartas.add(new Carta("8", "Ouros", 8));
        cartas.add(new Carta("8", "Espadas", 8));
        cartas.add(new Carta("8", "Copas", 8));
        cartas.add(new Carta("8", "Paus", 8));

        cartas.add(new Carta("9", "Ouros", 9));
        cartas.add(new Carta("9", "Espadas", 9));
        cartas.add(new Carta("9", "Copas", 9));
        cartas.add(new Carta("9", "Paus", 9));

        cartas.add(new Carta("10", "Ouros", 10));
        cartas.add(new Carta("10", "Espadas", 10));
        cartas.add(new Carta("10", "Copas", 10));
        cartas.add(new Carta("10", "Paus", 10));

        cartas.add(new Carta("J", "Ouros", 10));
        cartas.add(new Carta("J", "Espadas", 10));
        cartas.add(new Carta("J", "Copas", 10));
        cartas.add(new Carta("J", "Paus", 10));

        cartas.add(new Carta("Q", "Ouros", 10));
        cartas.add(new Carta("Q", "Espadas", 10));
        cartas.add(new Carta("Q", "Copas", 10));
        cartas.add(new Carta("Q", "Paus", 10));

        cartas.add(new Carta("K", "Ouros", 10));
        cartas.add(new Carta("K", "Espadas", 10));
        cartas.add(new Carta("K", "Copas", 10));
        cartas.add(new Carta("K", "Paus", 10));
    }

    public void embaralhar(){
        Collections.shuffle(cartas);
    }
    public Carta sacarCarta(){
        return cartas.pollLast();
    }
}
