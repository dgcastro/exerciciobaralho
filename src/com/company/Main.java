package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        BlackJack blackJack = new BlackJack("Danilo");
        int decisao;
        Scanner scanner = new Scanner(System.in);
        do {
            blackJack.novaRodada();
            System.out.println("Seu melhor resultado até o momento foi de: "+blackJack.getMelhorRodada().getValor()+" pontos");
            System.out.println("Deseja jogar mais uma rodada?\nPressione 1 para jogar ou 0 para sair");
            decisao = scanner.nextInt();
        } while (decisao == 1);
    }
}
