package com.company;

import java.util.LinkedList;

public class Rodada {
    private Baralho baralho;
    private LinkedList<Carta> cartasNaMao;
    private int valor;

    public Rodada() {
        this.baralho = new Baralho();
        baralho.embaralhar();

        cartasNaMao = new LinkedList<Carta>();
        cartasNaMao.add(baralho.sacarCarta());
        cartasNaMao.add(baralho.sacarCarta());

        valor = 0;

        System.out.println("Suas cartas são:");
        for (Carta carta: cartasNaMao
             ) {
            System.out.println(carta.getFigura()+" de "+carta.getNaipe());
            valor += carta.getValor();
        }
        System.out.println("Você tem: " + valor + " pontos");
    }
    public Rodada(int valor){
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public boolean hit(){
        Carta carta = baralho.sacarCarta();
        System.out.println("A carta comprada foi: "+carta.getFigura()+" de "+carta.getNaipe());
        valor+=carta.getValor();
        if(valor <= 21){
            return true;
        }else
            return false;
    }
}
