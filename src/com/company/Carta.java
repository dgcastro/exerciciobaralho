package com.company;

public class Carta {
    private String figura;
    private String naipe;
    private int valor;

    public Carta(String figura, String naipe, int valor) {
        this.figura = figura;
        this.naipe = naipe;
        this.valor = valor;
    }

    public String getFigura() {
        return figura;
    }

    public String getNaipe() {
        return naipe;
    }

    public int getValor() {
        return valor;
    }
}
